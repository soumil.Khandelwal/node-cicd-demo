
# Look on the Docker hub with the node version specified running on linux alpine
FROM node:12.16.1-alpine

# Make a working diretory
WORKDIR /app
# Copy the package file in that directory
COPY package.json /app
# Install npm there (from the package.json)
RUN npm install
# Copy My application there
COPY . /app
# Run the Node server
CMD node server.js

# Expose the port where our app is running
EXPOSE 8080
